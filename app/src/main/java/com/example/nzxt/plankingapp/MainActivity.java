package com.example.nzxt.plankingapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //slider
    private HashMap<String, Integer> image_list;
    private SliderLayout mSlider;
    private ImageButton btnResume, btnExersices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSlider = (SliderLayout) findViewById(R.id.slider);
        btnExersices = (ImageButton) findViewById(R.id.btnStart);
        btnResume = (ImageButton) findViewById(R.id.btnSamenvatting);

        image_list = new HashMap<>();

        setupSlider();

        btnResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ResumeActivity.class);
                startActivity(intent);
            }
        });

        btnExersices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ExercisesActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupSlider() {

        image_list.put("How To", R.drawable.start);
        image_list.put("Full Plank", R.drawable.fullplank);
        image_list.put("Elbow Plank", R.drawable.elbowplank);
        image_list.put("Puchup Plank", R.drawable.puchupplank);
        image_list.put("How To", R.drawable.noyes);

        for (String key : image_list.keySet()) {

            //create slider
            final TextSliderView textSliderView = new TextSliderView(getBaseContext());
            textSliderView
                    .description(key)
                    .image(image_list.get(key))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                        }
                    });
            //add extra bundle
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", key);

            mSlider.addSlider(textSliderView);
        }
        mSlider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSlider.setCustomAnimation(new DescriptionAnimation());
        mSlider.setDuration(4000);

    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mSlider.stopAutoCycle();
        super.onStop();
    }
}
