package com.example.nzxt.plankingapp.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nzxt.plankingapp.R;
import com.example.nzxt.plankingapp.model.DataModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NZXT on 8-3-2018.
 */

public class CustomListViewResumeAdapter extends ArrayAdapter<DataModel> {

    private ArrayList<DataModel> dataSet;
    Context mContext;

    public CustomListViewResumeAdapter(@NonNull Context context, ArrayList<DataModel> dataSet) {
        super(context, R.layout.list_item, dataSet);
        this.dataSet = dataSet;
        this.mContext = context;
    }

    private int lastPosition = -1;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        DataModel dataModel = getItem(position);

        CustomListViewResumeAdapter.ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new CustomListViewResumeAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_resume, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.tvResume);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.ivImage_resume);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CustomListViewResumeAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(dataModel.getName());
        Picasso.with(mContext).load(dataModel.getImagePath()).into(viewHolder.img);

        return convertView;
    }

    private static class ViewHolder {
        TextView txtName;
        ImageView img;

    }
}
