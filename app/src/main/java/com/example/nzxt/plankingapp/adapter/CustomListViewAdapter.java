package com.example.nzxt.plankingapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.nzxt.plankingapp.R;
import com.example.nzxt.plankingapp.model.DataModel;

import java.util.ArrayList;

/**
 * Created by NZXT on 7-3-2018.
 */

public class CustomListViewAdapter extends ArrayAdapter<DataModel> {

    private ArrayList<DataModel> dataSet;
    Context mContext;

    public CustomListViewAdapter(@NonNull Context context, ArrayList<DataModel> dataSet) {
        super(context, R.layout.list_item, dataSet);
        this.dataSet = dataSet;
        this.mContext = context;
    }

    private int lastPosition = -1;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        DataModel dataModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder.txtExercise = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.txtSeperator = (TextView) convertView.findViewById(R.id.tvSeperator);
            viewHolder.txtTime = (TextView) convertView.findViewById(R.id.tvTime);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtExercise.setText(dataModel.getName());
        viewHolder.txtSeperator.setText(dataModel.getSeperator());
        viewHolder.txtTime.setText(dataModel.getTime());
        return convertView;
    }

    private static class ViewHolder {
        TextView txtExercise;
        TextView txtSeperator;
        TextView txtTime;
    }
}
