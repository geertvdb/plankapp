package com.example.nzxt.plankingapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ankushgrover.hourglass.Hourglass;
import com.squareup.picasso.Picasso;

import java.util.List;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;

public class StartExerciseActivity extends AppCompatActivity {

    private TextView tvClock, tvDialogPause;
    private Button btnStart, btnPauze, btnSkip;
    private ImageView img;
    private CountDownTimer countDownTimer;
    private CountDownTimer countDownTimer2;
    private CircleProgressView mCircleView;
    private boolean isEasy;
    private TextView tvExerciseName;
    private int[] arrEasy = {30, 15, 15, 15, 15, 15, 15, 30};
    private int[] arrAdvanced = {60, 30, 30, 30, 30, 30, 30, 60};
    private int[] arrExpert = {90, 45, 45, 45, 45, 45, 45, 90};
    private int[] arrImage = {R.drawable.full_plank, R.drawable.elbow_plank, R.drawable.raised_leg_plank_left,
            R.drawable.raised_leg_plank_right, R.drawable.side_plank_left, R.drawable.side_plank_right,
            R.drawable.full_plank, R.drawable.elbow_plank};
    private String[] arrName = {"1. Full Plank", "2. Elbow Plank", "3. Raised leg plank left", "4. Raised leg plank right",
            "5. Side plank left", "6. Side plank right", "7. Full Plank", "8. Elbow Plank"};
    private int counter = 0;
    private int secondsExercise = 0;
    private long maxBarVal = 0;
    private LinearLayout llSingleButton;
    private LinearLayout llThreeButton;
    private Button btnFinish;
    private int pauzeCounter = 0;
    private boolean isClicked = true;
    private boolean isPaused;
    private boolean isCanceled;
    private long reminigTime = 0;
    private Hourglass hourglass;
    private MediaPlayer mediaPlayer = null;
    private boolean blnSkipButton = true;
    private String pause;
    private String niveau;
    private boolean blnSound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_exercise);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        pause = settings.getString("list_pauze", "60");
        niveau = settings.getString("list_level", "60");
        blnSound = settings.getBoolean("example_switch", true);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnPauze = (Button) findViewById(R.id.btnPauze);
        btnSkip = (Button) findViewById(R.id.btnSkip);

        btnSkip.setEnabled(false);
        btnPauze.setEnabled(false);
        tvExerciseName = (TextView) findViewById(R.id.tvExerciseName);
        img = (ImageView) findViewById(R.id.ivImage);
        llSingleButton = (LinearLayout) findViewById(R.id.llSingleButton);
        llThreeButton = (LinearLayout) findViewById(R.id.llThreeButton);
        btnFinish = (Button) findViewById(R.id.btnFinish);
        llSingleButton.setVisibility(View.GONE);
        llThreeButton.setVisibility(View.VISIBLE);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {
                Log.d("TAG", "Progress Changed: " + value);
            }
        });

        if (niveau.equals("30")) {
            secondsExercise = arrEasy[counter];
            tvExerciseName.setText(arrName[counter]);
            maxBarVal = secondsExercise;
            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
            mCircleView.setMaxValue(secondsExercise);
            mCircleView.setText("" + secondsExercise);
            mCircleView.setTextMode(TextMode.TEXT);
            mCircleView.setValue(secondsExercise);
            secondsExercise = secondsExercise * 1000;
        } else if(niveau.equals("60")){
            secondsExercise = arrAdvanced[counter];
            tvExerciseName.setText(arrName[counter]);
            maxBarVal = secondsExercise;
            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
            mCircleView.setMaxValue(secondsExercise);
            mCircleView.setText("" + secondsExercise);
            mCircleView.setTextMode(TextMode.TEXT);
            mCircleView.setValue(secondsExercise);
            secondsExercise = secondsExercise * 1000;
        }else{
            secondsExercise = arrExpert[counter];
            tvExerciseName.setText(arrName[counter]);
            maxBarVal = secondsExercise;
            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
            mCircleView.setMaxValue(secondsExercise);
            mCircleView.setText("" + secondsExercise);
            mCircleView.setTextMode(TextMode.TEXT);
            mCircleView.setValue(secondsExercise);
            secondsExercise = secondsExercise * 1000;
        }

        timer(secondsExercise, 1000);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //countDownTimer.start();
                hourglass.startTimer();
                blnSkipButton = true;
                btnStart.setEnabled(false);
                btnSkip.setEnabled(true);
                btnPauze.setEnabled(true);
                isCanceled = false;
                isPaused = false;
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartExerciseActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blnSkipButton = false;
                btnSkip.setEnabled(false);
                btnStart.setEnabled(true);
                btnPauze.setEnabled(false);
                if(hourglass.isRunning()){
                    hourglass.stopTimer();
                }
                if(counter >= 7){
                    blnSkipButton = false;
                }
                mCircleView.setText("0");
                if (niveau.equals("30")) {
                    if (counter < 7 ) {
                        counter++;
                        Log.d("TIMER", "" + counter);
                        secondsExercise = arrEasy[counter];
                        tvExerciseName.setText(arrName[counter]);
                        maxBarVal = secondsExercise;
                        Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                        mCircleView.setText(String.valueOf(secondsExercise));
                        mCircleView.setMaxValue(secondsExercise);
                        mCircleView.setValue(secondsExercise);
                        secondsExercise = secondsExercise * 1000;
                    } else {
                        mCircleView.setText("Finish");
                        llSingleButton.setVisibility(View.VISIBLE);
                        llThreeButton.setVisibility(View.GONE);
                    }
                } else if(niveau.equals("60")) {
                    if (counter < 7) {
                        counter++;
                        secondsExercise = arrAdvanced[counter];
                        tvExerciseName.setText(arrName[counter]);
                        maxBarVal = secondsExercise;
                        Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                        mCircleView.setText(String.valueOf(secondsExercise));
                        mCircleView.setMaxValue(secondsExercise);
                        mCircleView.setValue(secondsExercise);
                        secondsExercise = secondsExercise * 1000;
                    } else {
                        mCircleView.setText("Finish");
                        llSingleButton.setVisibility(View.VISIBLE);
                        llThreeButton.setVisibility(View.GONE);
                    }
                } else{
                    if (counter < 7) {
                        counter++;
                        secondsExercise = arrExpert[counter];
                        tvExerciseName.setText(arrName[counter]);
                        maxBarVal = secondsExercise;
                        Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                        mCircleView.setText(String.valueOf(secondsExercise));
                        mCircleView.setMaxValue(secondsExercise);
                        mCircleView.setValue(secondsExercise);
                        secondsExercise = secondsExercise * 1000;
                    } else {
                        mCircleView.setText("Finish");
                        llSingleButton.setVisibility(View.VISIBLE);
                        llThreeButton.setVisibility(View.GONE);
                    }
                }

                timer(secondsExercise, 1000);
            }
        });

        btnPauze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClicked) {
                    btnPauze.setText("Resume");
                    isClicked = false;
                    hourglass.pauseTimer();

                } else {
                    btnPauze.setText("Pause");
                    isClicked = true;
                    hourglass.resumeTimer();
                    if(!hourglass.isRunning()){
                        hourglass.resumeTimer();
                    }
                }
            }
        });
    }

    private void timer(int i, int j) {


        hourglass = new Hourglass(i, j) {
            @Override
            public void onTimerTick(long l) {
                // Update UI
                reminigTime = l;
                long seconds = l / 1000;
                reminigTime = seconds;
                long barVal = (maxBarVal) - ((int) (seconds / 60 * 100) + (int) (seconds % 60));
                mCircleView.setValue((int) barVal);

                mCircleView.setText("" + seconds);
                mCircleView.setTextMode(TextMode.TEXT);
            }

            @Override
            public void onTimerFinish() {
                if(blnSkipButton){
                    if(counter < 7){
                        showPauzeDialog();
                    }

                    btnStart.setEnabled(true);
                    btnPauze.setEnabled(false);
                    btnSkip.setEnabled(false);

                    mCircleView.setText("0");
                    if (niveau.equals("30")) {
                        if (counter < 7) {
                            counter++;
                            secondsExercise = arrEasy[counter];
                            tvExerciseName.setText(arrName[counter]);
                            maxBarVal = secondsExercise;
                            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                            mCircleView.setText(String.valueOf(secondsExercise));
                            mCircleView.setMaxValue(secondsExercise);
                            mCircleView.setValue(secondsExercise);
                            secondsExercise = secondsExercise * 1000;
                        } else {
                            mCircleView.setText("Finish");
                            llSingleButton.setVisibility(View.VISIBLE);
                            llThreeButton.setVisibility(View.GONE);
                        }


                    } else if(niveau.equals("60")){
                        if (counter < 7) {
                            counter++;
                            secondsExercise = arrAdvanced[counter];
                            tvExerciseName.setText(arrName[counter]);
                            maxBarVal = secondsExercise;
                            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                            mCircleView.setText(String.valueOf(secondsExercise));
                            mCircleView.setMaxValue(secondsExercise);
                            mCircleView.setValue(secondsExercise);
                            secondsExercise = secondsExercise * 1000;
                        } else {
                            mCircleView.setText("Finish");
                            llSingleButton.setVisibility(View.VISIBLE);
                            llThreeButton.setVisibility(View.GONE);
                        }
                    }else{
                        if (counter < 7) {
                            counter++;
                            secondsExercise = arrExpert[counter];
                            tvExerciseName.setText(arrName[counter]);
                            maxBarVal = secondsExercise;
                            Picasso.with(StartExerciseActivity.this).load(arrImage[counter]).into(img);
                            mCircleView.setText(String.valueOf(secondsExercise));
                            mCircleView.setMaxValue(secondsExercise);
                            mCircleView.setValue(secondsExercise);
                            secondsExercise = secondsExercise * 1000;
                        } else {
                            mCircleView.setText("Finish");
                            llSingleButton.setVisibility(View.VISIBLE);
                            llThreeButton.setVisibility(View.GONE);
                        }
                    }

                    timer(secondsExercise, 1000);
                }
            }
        };

    }

    private void showPauzeDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View forgot_view = inflater.inflate(R.layout.pauze_layout, null);

        builder.setView(forgot_view);

        final Button btnFinishPauze = (Button) forgot_view.findViewById(R.id.btnSkipPauze);
        final CircleProgressView mCircleView = (CircleProgressView) forgot_view.findViewById(R.id.circleViewPauze);
        tvDialogPause = (TextView)forgot_view.findViewById(R.id.textt);

        final AlertDialog dialog =  builder.create();
        btnFinishPauze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                countDownTimer2.cancel();
            }
        });

        final long timePause;
        if(pause.equals("5")){
             timePause = 5000;
             tvDialogPause.setText("Rest for 5 seconds");
        }else if (pause.equals("10")){
             timePause = 10000;
            tvDialogPause.setText("Rest for 10 seconds");
        }else if(pause.equals("20")){
             timePause = 20000;
            tvDialogPause.setText("Rest for 20 seconds");
        }else{
             timePause = 30000;
            tvDialogPause.setText("Rest for 30 seconds");
        }

        countDownTimer2 = new CountDownTimer(timePause, 1000) {
            @Override
            public void onTick(long l) {
                //    tvClock.setText("" + l/1000);
                long seconds = l / 1000;
                int time = (int)timePause/1000;
                int barVal = (time) - ((int) (seconds / 60 * 100) + (int) (seconds % 60));
                mCircleView.setValue((int) barVal);
                mCircleView.setMaxValue(time);

                mCircleView.setText("" + seconds);
                mCircleView.setTextMode(TextMode.TEXT);

            }

            @Override
            public void onFinish() {
                dialog.dismiss();
            }
        }.start();

        if(pauzeCounter < 7){
            Log.d("COUNTER", "rrrrrrr " + pauzeCounter);
            pauzeCounter++;
            dialog.show();
            if(blnSound){
                mediaPlayer = MediaPlayer.create(StartExerciseActivity.this, R.raw.fart);
                mediaPlayer.start();
            }

        }else{
            pauzeCounter = 0;
        }
    }

    @Override
    protected void onStop() {
        if( mediaPlayer != null && !mediaPlayer.isPlaying()){
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onStop();
    }
}
