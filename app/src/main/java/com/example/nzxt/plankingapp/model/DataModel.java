package com.example.nzxt.plankingapp.model;

/**
 * Created by NZXT on 7-3-2018.
 */

public class DataModel {

    private String name;
    private String seperator;
    private String time;
    private int imagePath;

    public DataModel() {
    }

    public DataModel(String name, int imagePath) {

        this.name = name;
        this.imagePath = imagePath;
    }

    public DataModel(String name, String seperator, String time) {
        this.name = name;
        this.seperator = seperator;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeperator() {
        return seperator;
    }

    public void setSeperator(String seperator) {
        this.seperator = seperator;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }
}
