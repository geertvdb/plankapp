package com.example.nzxt.plankingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.nzxt.plankingapp.adapter.CustomListViewResumeAdapter;
import com.example.nzxt.plankingapp.model.DataModel;

import java.util.ArrayList;

public class ResumeActivity extends AppCompatActivity {

    private ArrayList<DataModel> lstExercises;
    private ListView lstView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lstView = (ListView) findViewById(R.id.list_view_resume);
        lstExercises = new ArrayList<>();
        getData();

        CustomListViewResumeAdapter adapter = new CustomListViewResumeAdapter(getApplicationContext(), lstExercises);
        lstView.setAdapter(adapter);

    }

    private void getData(){
        lstExercises.add(new DataModel("Full Plank", R.drawable.full_plank));
        lstExercises.add(new DataModel("Elbow Plank", R.drawable.elbow_plank));
        lstExercises.add(new DataModel("Raised Leg Plank(Left)", R.drawable.raised_leg_plank_left));
        lstExercises.add(new DataModel("Raised Leg Plank(Right)", R.drawable.raised_leg_plank_right));
        lstExercises.add(new DataModel("Side Plank(Left)", R.drawable.side_plank_left));
        lstExercises.add(new DataModel("Side Plank(Right)", R.drawable.side_plank_right));
    }
}
