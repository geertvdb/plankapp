package com.example.nzxt.plankingapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nzxt.plankingapp.adapter.CustomListViewAdapter;
import com.example.nzxt.plankingapp.model.DataModel;

import java.util.ArrayList;
import java.util.List;

public class ExercisesActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private ListView listView;
    private ArrayList<DataModel> lstExercises;
    private ArrayList<DataModel> lstExercises30;
    private ArrayList<DataModel> lstExercises90;
    private CustomListViewAdapter adapter;
    private TextView tvNiveau, tvPause;
    private Button btnStart;
    private boolean isEasy = true;
    private String pause;
    private String niveau;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        pause = settings.getString("list_pauze", "60");
        niveau = settings.getString("list_level", "60");

        settings.registerOnSharedPreferenceChangeListener(this);

        btnStart = (Button) findViewById(R.id.btnStart);
        tvNiveau = (TextView)findViewById(R.id.tvNiveau);
        tvPause = (TextView)findViewById(R.id.tvPause);

        if(niveau.equals("30")){
            tvNiveau.setText("Beginner");
        }else if(niveau.equals("60")){
            tvNiveau.setText("Normal");
        }else{
            tvNiveau.setText("Expert");
        }

        tvPause.setText(pause + " " + "Seconden");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView)findViewById(R.id.listView);
        lstExercises = new ArrayList<>();
        lstExercises30 = new ArrayList<>();
        lstExercises90 = new ArrayList<>();

        lstExercises.add(new DataModel("Full Plank",":", "60 Seconds"));
        lstExercises.add(new DataModel("Elbow Plank", ":", "30 Seconds"));
        lstExercises.add(new DataModel("Raised Leg Plank\n(Left)", ":", "30 Seconds"));
        lstExercises.add(new DataModel("Raised Leg Plank\n(Right)",":", " 30 Seconds"));
        lstExercises.add(new DataModel("Side Plank(Left)", ":", "30 Seconds"));
        lstExercises.add(new DataModel("Side Plank(Right)", ":", "30 Seconds"));
        lstExercises.add(new DataModel("Full Plank", ":", "30 Seconds"));
        lstExercises.add(new DataModel("Elbow Plank",":", "60 Seconds"));

        lstExercises30.add(new DataModel("Full Plank",":", "30 Seconds"));
        lstExercises30.add(new DataModel("Elbow Plank", ":", "15 Seconds"));
        lstExercises30.add(new DataModel("Raised Leg Plank\n(Left)", ":", "15 Seconds"));
        lstExercises30.add(new DataModel("Raised Leg Plank\n(Right)",":", " 15 Seconds"));
        lstExercises30.add(new DataModel("Side Plank(Left)", ":", "15 Seconds"));
        lstExercises30.add(new DataModel("Side Plank(Right)", ":", "15 Seconds"));
        lstExercises30.add(new DataModel("Full Plank", ":", "15 Seconds"));
        lstExercises30.add(new DataModel("Elbow Plank",":", "30 Seconds"));

        lstExercises90.add(new DataModel("Full Plank",":", "90 Seconds"));
        lstExercises90.add(new DataModel("Elbow Plank", ":", "45 Seconds"));
        lstExercises90.add(new DataModel("Raised Leg Plank\n(Left)", ":", "45 Seconds"));
        lstExercises90.add(new DataModel("Raised Leg Plank\n(Right)",":", " 45 Seconds"));
        lstExercises90.add(new DataModel("Side Plank(Left)", ":", "45 Seconds"));
        lstExercises90.add(new DataModel("Side Plank(Right)", ":", "45 Seconds"));
        lstExercises90.add(new DataModel("Full Plank", ":", "45 Seconds"));
        lstExercises90.add(new DataModel("Elbow Plank",":", "90 Seconds"));


        if(niveau.equals("30")){
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises30);
            listView.setAdapter(adapter);
        }else if(niveau.equals("60")){
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises);
            listView.setAdapter(adapter);
        }else{
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises90);
            listView.setAdapter(adapter);
        }

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExercisesActivity.this, StartExerciseActivity.class);
                intent.putExtra("isEasy", isEasy);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(ExercisesActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(niveau.equals("30")){
            tvNiveau.setText("Beginner");
        }else if(niveau.equals("60")){
            tvNiveau.setText("Normal");
        }else{
            tvNiveau.setText("Expert");
        }

        tvPause.setText(pause + " " + "Seconden");

        if(niveau.equals("30")){
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises30);
            listView.setAdapter(adapter);
        }else if(niveau.equals("60")){
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises);
            listView.setAdapter(adapter);
        }else{
            CustomListViewAdapter adapter = new CustomListViewAdapter(getApplicationContext(), lstExercises90);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        recreate();
    }
}
